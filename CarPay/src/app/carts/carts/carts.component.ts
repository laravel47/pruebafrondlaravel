import { ServiceCartsService } from './../../servicios/service-carts.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carts',
  templateUrl: './carts.component.html',
  styleUrls: ['./carts.component.css']
})
export class CartsComponent implements OnInit {
  carrito: any;

  idProducto: string;
  quantity: string;
  id: string;
  nombre: string;
  descripcion: string;
  constructor(private service: ServiceCartsService) { }

  ngOnInit(): void {
    this.nuevoCart();
  }

  nuevoCart(): void{


    this.service.findProductsCart().subscribe(resultado => {
      console.log(resultado);
      this.carrito = resultado;
    });
  }

  editardetalle(row): void{
    console.log(row.product_id);
    this.id = row.id;
    this.idProducto = row.product_id;
    this.quantity = row.quantity;
    this.nombre = row.nombre;
    this.descripcion = row.descripcion;

    console.log(this.idProducto);
  }

  guardar(): void{
    console.log(this.idProducto);
    const fd = {
      product_id: this.idProducto,
      cart_id: this.service.getCar(),
      quantity: this.quantity
    };


    console.log(fd);
    this.service.updateProductsCart(this.id, fd).subscribe(resultado => {
      console.log(resultado);
      this.nuevoCart();
      //this.productos = resultado;
    });

  }

  eliminardetalle(row): void{
    this.id = row.id;
    this.idProducto = row.product_id;
    this.quantity = row.quantity;
    this.nombre = row.nombre;
    this.descripcion = row.descripcion;
  }

  eliminar(): void{
    this.service.deleteProductsCart(this.id).subscribe(resultado => {
      console.log(resultado);
      this.nuevoCart();
      //this.productos = resultado;
    });
  }

  finalizar(): void{
    const fd = {status: 'completed'};
    const id = this.service.getCar();
    this.service.updateCar(id, fd).subscribe(resultado => {
      window.location.reload();
    });
  }

}
