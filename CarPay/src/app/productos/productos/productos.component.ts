import { ServiceCartsService } from './../../servicios/service-carts.service';
import { ServiceProductosService } from './../../servicios/service-productos.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  constructor(private service: ServiceProductosService, private serviceCar: ServiceCartsService) { }
  productos: any;
  headers = ['id', 'nombre', 'sku', 'descripcion'];

  p: number = 1;
  total: number;

  view: boolean;
  btntitel: string;
  idProducto: string;
  nombre: string;
  sku: string;
  descripcion: string;
  quantity: string;
  ngOnInit(): void {
    this.view = false;
    this.btntitel = 'Ver Carrito';
    this.getProductos();
    this.nuevoCar();
  }

  verCarrito(): void{
    if (this.view){
      this.view = false;
      this.btntitel = 'Ver Carrito';
    }else{
      this.view = true;
      this.btntitel = 'Ocultar Carrito';
    }

  }

  getProductos(): void{
    this.service.getAllProductos().subscribe(resultado => {
      console.log(resultado);
      this.productos = resultado;
    });
  }

  agregar(row): void{
    this.idProducto = row.id;
    this.nombre = row.nombre;
    this.sku = row.sku;
    this.descripcion = row.descripcion;
  }

  nuevoCar(): void{

    const fd = new FormData();

    fd.append('status', 'pending');
    this.serviceCar.RegistrarCar(fd).subscribe(resultado => {
      console.log(resultado['id']);
    });
  }

  guardar(): void{
    const fd = new FormData();

    fd.append('product_id', this.idProducto);
    fd.append('cart_id', this.serviceCar.getCar());
    fd.append('quantity', this.quantity);
    this.serviceCar.addProduct(fd).subscribe(resultado => {
      console.log(resultado);
      //this.productos = resultado;
    });
  }

}
