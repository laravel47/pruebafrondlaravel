import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap, timestamp} from 'rxjs/Operators';

@Injectable({
  providedIn: 'root'
})
export class ServiceCartsService {

  constructor(private client: HttpClient, private cookies: CookieService) { }

  private api = environment.host + 'carts';
  private apiRel = environment.host + 'productosCars';


  RegistrarCar(datos: object): Observable<any>{

    console.log(datos);
    return this.client.post(this.api, datos)
    .pipe(
      tap(
        (res) => {
          this.setCar(res['id'], res['status']);
        }
      )
    );
  }

  setCar(id: string, status: string): void{
    this.cookies.set('car', id);
    this.cookies.set('status', status);
  }

  getCar(): string {
    return this.cookies.get('car');
  }

  updateCar(id: string, datos: object): Observable<any>{
    const header = new HttpHeaders()
    .set('Content-Type', 'application/json' )
    .set('accept', 'application/json');
    return this.client.put(this.api + '/' + id, JSON.stringify(datos), {headers: header});
  }


  findProductsCart(): Observable<any>{
    return this.client.get(this.apiRel + '/' + this.getCar());
  }

  addProduct(datos: object): Observable<any>{
    return this.client.post(this.apiRel, datos);
  }

  updateProductsCart(id: string, datos: object): Observable<any>{
    console.log(datos);
    const header = new HttpHeaders()
    .set('Content-Type', 'application/json' )
    .set('accept', 'application/json');

    return this.client.put(this.apiRel + '/' + id, JSON.stringify(datos), {headers: header});
  }

  deleteProductsCart(id: string): Observable<any>{
    return this.client.delete(this.apiRel + '/' + id);
  }

}
