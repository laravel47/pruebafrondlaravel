import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceProductosService {

  constructor(private client: HttpClient) { }

  private api = environment.host + 'productos';

  getAllProductos(): Observable<any>{
    return this.client.get(this.api);
  }
}
