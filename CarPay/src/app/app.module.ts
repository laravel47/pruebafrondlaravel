import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {HttpClientModule} from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import {MatIconModule} from '@angular/material/icon';

import { CookieService } from 'ngx-cookie-service';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProductosComponent } from './productos/productos/productos.component';
import { DocumentacionComponent } from './docs/documentacion/documentacion.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CartsComponent } from './carts/carts/carts.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductosComponent,
    DocumentacionComponent,
    CartsComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    HttpClientModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    BrowserAnimationsModule
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
